<?php

/**
 * Defines application features from the specific context.
 */
class FeatureContext extends AttractorContext
{
    /**
     * @When /^я вижу слово "([^"]*)" где\-то на странице$/
     */
    public function яВижуСловоГдеТоНаСтранице($arg1)
    {
        $this->assertPageContainsText($arg1);
    }

    /**
     * @When /^я нахожусь на главной странице$/
     */
    public function яНахожусьНаГлавнойСтранице()
    {
        $this->visit($this->getContainer()->get('router')->generate('homepage'));
    }

    /**
     * @When /^я перехожу на страницу регистрации$/
     */
    public function яПерехожуНаСтраницуРегистрации()
    {
        $this->visit($this->getContainer()->get('router')->generate('register'));
    }

    /**
     * @When /^я заполняю форму регистрации и регистрируюсь$/
     */
    public function яЗаполняюФормуРегистрацииИРегистрируюсь()
    {
        $this->fillField('app_client_username', uniqid() . "landlordtest@gmail.com");
        $this->fillField('app_client_password_first', "qwerty");
        $this->fillField('app_client_password_second', "qwerty");
        $this->pressButton('app_client_save');
    }


    /**
     * @When /^я авторизуюсь с логином "([^"]*)" и паролем "([^"]*)"/
     */
    public function яАвторизусьСЛогиномИПаролем($login, $password)
    {
        $this->fillField('app_client_username', $login);
        $this->fillField('app_client_password', $password);
        $this->pressButton('app_client_login');
    }

    /**
     * @When /^я использую поиск и ищу тэг "([^"]*)"/
     */
    public function яИспользуюПоиск($text)
    {
        $this->fillField('app_client_search', $text);
        $this->selectOption('app_client_sort', 'new');
        $this->selectOption('app_client_number', '8');
        $this->pressButton('app_client_go');
        sleep(5);
    }

    /**
     * @When /^я кликаю по ссылке поста и добавляю в избраное/
     */
    public function яКликаюПоСсылкеПостаИДобавляюВИзбраное()
    {
        $this->getSession()->getPage()->find('css', '.post-test')->click();
        sleep(5);
        $this->getSession()->getPage()->find('css', '.btn')->click();
        sleep(5);
    }

    /**
     * @When /^я перехожу на страницу моих избранных/
     */
    public function яПерехожуНаСтраницуМоихИзбранных()
    {
        $this->visit($this->getContainer()->get('router')->generate('my-favorites'));
    }

    /**
     * @When /^я перехожу на страницу социальные избранные/
     */
    public function яПерехожуНаСтраницуСоциальныеИзбраные()
    {
        $this->visit($this->getContainer()->get('router')->generate('get-hot'));
    }

    /**
     * @When /^я перехожу на страницу авторизации/
     */
    public function яПерехожуНаСтраницуАвторизации()
    {
        $this->visit($this->getContainer()->get('router')->generate('login'));
    }

    /**
     * @When /^я перехожу на страницу создания объекта/
     */
    public function яПерехожуНаСтраницуСозданияОбъекта()
    {
        $this->visit($this->getContainer()->get('router')->generate('add-new-object'));
    }

    /**
     * @When /^я заполняю форму регистрации объекта$/
     */
    public function яЗаполняюФормуРегистрацииОбъекта()
    {
        $this->fillField('app_client_name', uniqid());
        $this->fillField('app_client_numberOfRooms', "20");
        $this->fillField('app_client_contact', "0555123456");
        $this->fillField('app_client_rates', "2000");
        $this->selectOption('app_client_type', 'cottage');
        $this->pressButton('app_client_next');
        $this->checkOption('app_client_kitchen');
        $this->checkOption('app_client_terrace');
        $this->pressButton('app_client_next');


    }
}

