<?php


namespace App\DataFixtures;


use App\Entity\Favorite;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class FavoriteFixture extends Fixture implements DependentFixtureInterface
{
    private $names = ["t3_8wgone", "t3_8qc84c", "t3_8ouvwv", "t3_8sh3vu",
        "t3_8widua", "t3_8qaaoq", "t3_7x1mc1", "t3_8nr3ju", "t3_8gncs3",
        "t3_8qep03", "t3_8uwp0l", "t3_8talwl", "t3_7qbx7x", "t3_8narht",
        "t3_8sqpki", "t3_84nz9p", "t3_8l8nkv", "t3_8tw1q8", "t3_8hn9ee", "
        t3_8tagbq", "t3_8g1giq", "t3_8rbahm", "t3_7x1q34", "t3_8wld54",
        "t3_7du4tc", "t3_8tr6z7", "t3_8v27o0", "t3_8s7w5q", "t3_8ltpyx",
        "t3_8cgj3g", "t3_8rljbk", "t3_8srnfn", "t3_7cajyc", "t3_89b9v1",
        "t3_8c3956", "t3_8pp6sn", "t3_8uh3gd", "t3_8rbn86", "t3_7t3odz",
        "t3_8hhbqw", "t3_8ogmnm", "t3_8t52ic", "t3_78nc2a", "t3_853hp7",
        "t3_8pcd57", "t3_8u0hhy", "t3_8ljoz5", "t3_8vs2cu", "t3_7h3brc",
        "t3_8fil8u"];

    public function load(ObjectManager $manager)
    {
        foreach ($this->names as $key => $name) {
            $favorite = new Favorite();
            $favorite->setUser($this->getReference(UserFixtures::USER_ONE));
            $favorite->setPost($name);
            $manager->persist($favorite);

            if ($key < 9) {
                $favorite = new Favorite();
                $favorite->setUser($this->getReference(UserFixtures::USER_TWO));
                $favorite->setPost($name);
                $manager->persist($favorite);
            }

            if ($key < 5) {
                $favorite = new Favorite();
                $favorite->setUser($this->getReference(UserFixtures::USER_THREE));
                $favorite->setPost($name);
                $manager->persist($favorite);
            }


        }
        $manager->flush();
    }

    /**
     * This method must return an array of fixtures classes
     * on which the implementing class depends on
     *
     * @return array
     */
    function getDependencies()
    {
        return array(
            UserFixtures::class,
        );
    }
}