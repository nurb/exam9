<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Model\User\UserHandler;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    /**
     * @var UserHandler
     */
    private $userHandler;

    public const USER_ONE = 'Morty';
    public const USER_TWO = 'Rick';
    public const USER_THREE = 'Taylor';

    public function __construct(UserHandler $userHandler)
    {
        $this->userHandler = $userHandler;
    }

    public function load(ObjectManager $manager)
    {
        $user1 = new User();
        $user1->setUsername('rick');
        $user1->setPassword($this->userHandler->encodePlainPassword('qwerty'));
        $user1->setRoles(["ROLE_USER"]);
        $manager->persist($user1);

        $user2 = new User();
        $user2->setUsername('morty');
        $user2->setPassword($this->userHandler->encodePlainPassword('qwerty'));
        $user2->setRoles(["ROLE_USER"]);
        $manager->persist($user2);

        $user3 = new User();
        $user3->setUsername('taylor');
        $user3->setPassword($this->userHandler->encodePlainPassword('qwerty'));
        $user3->setRoles(["ROLE_USER"]);
        $manager->persist($user3);


        $this->addReference(self::USER_ONE, $user1);
        $this->addReference(self::USER_TWO, $user2);
        $this->addReference(self::USER_THREE, $user3);
        $manager->flush();
    }
}
