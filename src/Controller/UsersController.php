<?php

namespace App\Controller;

use App\Entity\User;
use App\Model\Api\ApiContext;
use App\Model\Api\ApiException;
use App\Model\User\UserHandler;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;


/**
 * Users controller.
 *
 * @Route("user")
 */
class UsersController extends Controller
{
    /**
     * @Route("/register", name="register")
     * @param Request $request
     * @param ObjectManager $manager
     * @param UserHandler $userHandler
     * @return Response
     */
    public function registerAction(
        Request $request,
        ObjectManager $manager,
        UserHandler $userHandler
    )
    {
        $user = new User();
        $form = $this->createForm("App\Form\RegisterType", $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $password = $userHandler->encodePlainPassword($user->getPassword());
            $user->setPassword($password);
            $manager->persist($user);
            $manager->flush();
            $this->addFlash(
                'notice',
                'Позравляем, ваша учетная успешно создана, пожалуйста авторизируйтесь'
            );
            return $this->redirectToRoute("login");
        }

        return $this->render('users/registration.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/login", name="login")
     * @param UserRepository $userRepository
     * @param Request $request
     * @param UserHandler $userHandler
     * @return Response
     */
    public function loginAction(
        UserRepository $userRepository,
        Request $request,
        UserHandler $userHandler
    )
    {
        $form = $this->createForm("App\Form\LoginType");
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $client = $userRepository->getByCredentials(
                $data->getUserName(),
                $data->getPassword()
            );

            if ($client) {
                $userHandler->makeUserSession($client);
                return $this->redirectToRoute('homepage');
            } else {
                $this->addFlash(
                    'error',
                    'Логин и пароль неверные'
                );
            }
        }
        return $this->render(
            'users/login.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Route("/profile", name="profile")
     * @return Response
     */
    public function profileAction()
    {
        $user = $this->getUser();
        return $this->render('users/profile.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/logout", name="logout")
     */
    public
    function logout()
    {

    }


}



