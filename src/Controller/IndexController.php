<?php

namespace App\Controller;


use App\Entity\Favorite;
use App\Model\Api\ApiContext;

use App\Model\Api\ApiException;
use App\Model\Posts\Post;
use App\Model\Posts\PostsHandler;
use App\Repository\FavoriteRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends Controller
{

    /**
     * @Route("/", name="homepage")
     * @param Request $request
     * @param ApiContext $apiContext
     * @param PostsHandler $postsHandler
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, ApiContext $apiContext, PostsHandler $postsHandler, FavoriteRepository $favoriteRepository)
    {
        $form = $this->createForm("App\Form\SearchType");
        /** @var  $allPosts Post[] */
        $allPosts = [];

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $response = null;
            try {
                $response = $apiContext->searchImages($data['search'], $data['search'], $data['number']);
            } catch (ApiException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()
                );
            }
            $allPosts = $postsHandler->parseRedditJson($response, $favoriteRepository);

        }

        return $this->render('homepage.html.twig', [
            "form" => $form->createView(),
            "posts" => $allPosts
        ]);
    }

    /**
     * @Route("/post/{name}", name="get-post")
     * @param string $name
     * @param Request $request
     * @param ApiContext $apiContext
     * @param PostsHandler $postsHandler
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getPostAction(string $name, Request $request, ApiContext $apiContext, PostsHandler $postsHandler, FavoriteRepository $favoriteRepository)
    {
        $post = null;
        $response = null;
        try {
            $response = $apiContext->getPost($name);
        } catch (ApiException $e) {
            $this->addFlash(
                'error',
                $e->getMessage()
            );
        }

        $post = $postsHandler->parseRedditJson($response, $favoriteRepository);

        return $this->render('post/post.html.twig', [
            'post' => $post[0]
        ]);
    }

    /**
     * @Route("/favorites/add/{post_id}", name="add-favorites")
     * @param string $post_id
     * @param Request $request
     * @param ObjectManager $manager
     * @param FavoriteRepository $favoriteRepository
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function addFavoritesAction(string $post_id, Request $request, ObjectManager $manager, FavoriteRepository $favoriteRepository)
    {

        if ($favoriteRepository->isFavoriteExists($this->getUser() , $post_id)) {
            $this->addFlash(
                'error',
                'Пост уже находится в избранном'
            );
        } else {
            $favorite = new Favorite();
            $favorite->setUser($this->getUser());
            $favorite->setPost($post_id);
            $manager->persist($favorite);
            $manager->flush();

            $this->addFlash(
                'notice',
                'Пост добавлен в избранное'
            );

        }


        return $this->redirect($request->headers->get('referer'));
    }


    /**
     * @Route("/favorites/my", name="my-favorites")
     * @param Request $request
     * @param ObjectManager $manager
     * @param ApiContext $apiContext
     * @param UserRepository $userRepository
     * @param PostsHandler $postsHandler
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function myFavoritesAction(Request $request, ObjectManager $manager, ApiContext $apiContext, UserRepository $userRepository, PostsHandler $postsHandler, FavoriteRepository $favoriteRepository)
    {
        $response = null;
        $list = null;
        $pagination = null;

        $favorites = $favoriteRepository->getFavoritesByUser($this->getUser());

        if ($favorites){
            foreach ($favorites as $favorite) {
                $list .= $favorite->getPost() . ',';
            }
            try {
                $response = $apiContext->getPost($list);
            } catch (ApiException $e) {
                $this->addFlash(
                    'error',
                    $e->getMessage()

                );
            }
            $allPosts = $postsHandler->parseRedditJson($response, $favoriteRepository);
            $paginator = $this->get('knp_paginator');
            $pagination = $paginator->paginate(
                $allPosts, /* query NOT result */
                $request->query->getInt('page', 1)/*page number*/,
                8/*limit per page*/
            );

        }

        return $this->render('post/my_favorites.html.twig', [
            'posts' => $pagination
        ]);
    }

    /**
     * @Route("/hot", name="get-hot")
     * @param Request $request
     * @param ObjectManager $manager
     * @param ApiContext $apiContext
     * @param UserRepository $userRepository
     * @param PostsHandler $postsHandler
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getHotAction(Request $request, ObjectManager $manager, ApiContext $apiContext, UserRepository $userRepository, PostsHandler $postsHandler, FavoriteRepository $favoriteRepository)
    {
        $response = null;
        $list = null;

        $favorites = $favoriteRepository->getAll();
        foreach ($favorites as $favorite) {

            $list .= $favorite["post"] . ',';
        }
        try {
            $response = $apiContext->getPost($list);
        } catch (ApiException $e) {
            $this->addFlash(
                'error',
                $e->getMessage()

            );
        }
        $allPosts = $postsHandler->parseRedditJson($response, $favoriteRepository);


        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $allPosts, /* query NOT result */
            $request->query->getInt('page', 1)/*page number*/,
            8/*limit per page*/
        );

        return $this->render('post/hot.html.html.twig', [
            'posts' => $pagination
        ]);
    }

}
