<?php

namespace App\Repository;


use App\Entity\User;
use App\Model\User\UserHandler;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository
{

    /**
     * @var UserHandler
     */
    private $clientHandler;

    public function __construct(
        RegistryInterface $registry,
        UserHandler $clientHandler
    )
    {
        parent::__construct($registry, User::class);
        $this->clientHandler = $clientHandler;
    }

    /**
     * @param string $username
     * @param string $plainPassword
     * @return User|null
     */
    public function getByCredentials(string $username, string $plainPassword)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.username = :username')
                ->andWhere('a.password = :password')
                ->setParameter(
                    'password',
                    $this->clientHandler->encodePlainPassword($plainPassword)
                )
                ->setParameter('username', $username)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }


    public function findOneByEmail($email): ?User
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.email= :email')
                ->setParameter('email', $email)
                ->setMaxResults(1)
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

}