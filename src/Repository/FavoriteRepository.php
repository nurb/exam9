<?php

namespace App\Repository;


use App\Entity\Favorite;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method Favorite|null find($id, $lockMode = null, $lockVersion = null)
 * @method Favorite|null findOneBy(array $criteria, array $orderBy = null)
 * @method Favorite[]    findAll()
 * @method Favorite[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class FavoriteRepository extends ServiceEntityRepository
{
    public function __construct(
        RegistryInterface $registry
    )
    {
        parent::__construct($registry, Favorite::class);
    }

    public function getFavoritesByUser($user)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.user = :user')
                ->setParameter('user', $user)
                ->orderBy('a.date', 'desc')
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }

    }

    public function getAll()
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a.post, COUNT(a.user) AS HIDDEN b')
                ->groupBy('a.post')
                ->orderBy('b', "desc")
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }

    }

    public function getAll_Two()
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->where('a.user IS NOT NULL')
                ->orderBy('a.date', 'desc')
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function countFavorites($post)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('COUNT(a)')
                ->where('a.post = :post')
                ->setParameter('post', $post)
                ->orderBy('a.date', 'desc')
                ->getQuery()
                ->getOneOrNullResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

    public function isFavoriteExists($user, $post)
    {
        try {
            return $this->createQueryBuilder('a')
                ->select('a')
                ->Where('a.user = :user')
                ->setParameter('user', $user)
                ->andWhere('a.post = :post')
                ->setParameter('post', $post)
                ->getQuery()
                ->getResult();
        } catch (NonUniqueResultException $e) {
            return null;
        }
    }

}