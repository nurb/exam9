<?php

namespace App\Form;


use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SearchType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('search', TextType::class, array(
                'label' => 'Search',
                'attr' => array(
                    'placeholder' => 'What we are looking for?',
                    "class" => "ml-2 mr-2"
                ),
            ))
            ->add('sort', ChoiceType::class, [
                'label' => false,
                'placeholder' => 'Select sort',
                'choices' => [
                    'Relevance' => 'relevance',
                    'Hot' => 'hot',
                    'Top' => 'top',
                    'New' => 'new',
                    'Most Comments' => 'comments'

                ], 'attr' => array(
                    'placeholder' => 'What we are looking for?',
                    "class" => "ml-2 mr-2"
                )

            ])
            ->add('number', ChoiceType::class, [
                'label' => false,
                'placeholder' => 'How many posts?',
                'choices' => [
                    '8 posts' => '8',
                    '12 posts' => '12',
                    '16 posts' => '16',
                    '50 posts' => '50',
                    '100 post' => '100'
                ], 'attr' => array(
                    'placeholder' => 'What we are looking for?',
                    "class" => "ml-2 mr-2"
                )

            ])
            ->add('go', SubmitType::class,
                array('label' => 'Go',
                    'attr' => ["class" => "btn btn-success ml-2 mr-2"]
                ));

    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'app_client';
    }


}
