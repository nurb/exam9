<?php

namespace App\Model\Posts;


use App\Model\Api\ApiException;
use App\Repository\FavoriteRepository;

class PostsHandler
{

    public function parseRedditJson($response, FavoriteRepository $repository)
    {
        $allPosts = [];
        foreach ($response['data']['children'] as $rawPost) {
            $post = new Post();
            $post->setName($rawPost['data']['name']);
            $post->setTitle($rawPost['data']['title']);

            $thumb = $rawPost['data']['thumbnail'];
            $image = $rawPost['data']['url'];
            if ($thumb == 'default' || $thumb == 'self') {
                $thumb = 'http://localhost:8000/images/no-image.png';
            }
            if ($image == 'default' || $thumb == 'self') {
                $image = 'http://localhost:8000/images/no-image.png';
            }
            $post->setThumbnail($thumb ?: 'http://localhost:8000/images/no-image.png');
            $post->setAuthor($rawPost['data']['author']);
            $post->setCreatedAt($rawPost['data']['created']);
            $post->setImage($image ?: 'http://localhost:8000/images/no-image.png');

            $post->setSubscriber($repository->countFavorites($post->getName()));
            $allPosts[] = $post;
        }
        return $allPosts;
    }

}