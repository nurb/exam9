<?php

namespace App\Model\Api;

class ApiContext extends AbstractApiContext
{
    const ENDPOINT_SEARCH = '/r/picture/search.json';
    const ENDPOINT_POST = '/api/info.json';

    /**
     * @param string $phrase
     * @param string $sort
     * @param string $limit
     * @return bool
     */
    public function searchImages(string $phrase, string $sort, string $limit)
    {
        return $this->makeQuery(self::ENDPOINT_SEARCH, self::METHOD_GET, [
            'q' => $phrase,
            'sort' => $sort,
            'limit' => $limit,
            'type' => 'link'
        ]);
    }

    public function getPost(string $name)
    {
        return $this->makeQuery(self::ENDPOINT_POST, self::METHOD_GET, [
            'id' => $name,
        ]);
    }

}
